# Local Library Catalog App

## Description
The purpose of this project is to manage the catalog of a local library. It's a slightly enhanced version of the MDN "Local Library" [Django tutorial](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Tutorial_local_library_website).


## Features
##### It allows an anonymous user to browse the catalog:
 - see the lists of books and their status
 - see the list of authors

##### A logged-in user belonging to the "Library Members" user group can:
- borrow a book (if it's available)
- reserve a book (if it's available or loaned to someone else)
- cancel a reservation
- see the list of their own borrowed books
- see the list of their own reservations

##### A logged-in user belonging to the "Librarians" user group can:
- see the list of borrowed books by all readers
- renew (once) a book loan
- mark a book instance as "returned"
- add, update or delete authors in the catalog

## Screenshots

##### Anonymous user:
Home page:

![home page](screenshots/home.png)


All authors:

![all authors](screenshots/all_authors.png)


All books:

![all books](screenshots/all_books.png)


Author detail:

![author detail](screenshots/author_detail.png)


Book detail:

![book detail](screenshots/book_detail_1.png)

---

##### Library Member:

Book detail logged-in as Library Member:

![book detail logged](screenshots/book_detail_logged1.png)


Another book detail logged-in as Library Member:

![book detail logged](screenshots/book_detail_logged.png)


Library member borrowed books:

![book detail logged](screenshots/my_borrowed.png)


Library member reservations:

![book detail logged](screenshots/my_reserved.png)


Borrowing a new book:

![book detail logged](screenshots/borrowing.png)


---

##### Librarian:

All authors logged-in as Librarian:

![book detail logged](screenshots/all_authors_librarian.png)


All loaned books:

![book detail logged](screenshots/all_borrowed.png)


Renew a book loan:

![book detail logged](screenshots/renew.png)

---

## Licence
This project is under MIT Licence.

