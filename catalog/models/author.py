import datetime

from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


class Author(models.Model):
    """Model representing an author."""
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.DateField('Born', null=True, blank=True)
    date_of_death = models.DateField('Died', null=True, blank=True)

    class Meta:
        ordering = ['last_name', 'first_name']

    def get_absolute_url(self):
        """Returns the url to access a particular author instance."""
        return reverse('author-detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return f'{self.last_name}, {self.first_name}'

    def clean(self):
        super().clean()
        if self.date_of_death and self.date_of_birth and self.date_of_birth > self.date_of_death:
            raise ValidationError(message=_('Date of birth cannot be after date of Death'), code='invalid')
        if self.date_of_death and self.date_of_death > datetime.date.today():
            raise ValidationError(message=_('Date of Death cannot be in the future'), code='invalid')
        if self.date_of_birth and self.date_of_birth > datetime.date.today():
            raise ValidationError(message=_('Date of Birth cannot be in the future'), code='invalid')