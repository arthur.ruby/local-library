from datetime import date
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.db import models
from django.contrib.auth.models import User
import uuid


class BookInstance(models.Model):
    """Model representing a specific physical copy of a book (i.e. that can be borrowed from the library)."""

    # UUIDField is used for the id field to set it as the primary_key for this model.
    # This type of field allocates a globally unique value for each instance
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          help_text='Unique ID for this particular book across whole library')

    # One book can have multiple instances in the library
    book = models.ForeignKey('Book', on_delete=models.SET_NULL, null=True)
    imprint = models.CharField(max_length=200)
    due_back = models.DateField(null=True, blank=True)
    borrower = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='borrower')
    reserver = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='reserver')
    has_been_renewed = models.BooleanField(default=False)

    LOAN_STATUS = (
        ('m', 'Maintenance'),
        ('l', 'Loaned'),
        ('a', 'Available'),
        ('r', 'Reserved'),
    )

    status = models.CharField(
        max_length=1,
        choices=LOAN_STATUS,
        blank=True,
        default='m',
        help_text='Book availability',
    )

    # You can declare model-level metadata for your Model by declaring class Meta.
    # One of the most useful features of this metadata is to control the
    # default ordering of records returned when you query the model type.
    class Meta:
        ordering = ['due_back']
        permissions = (("can_mark_returned", "Set book as returned"),
                       ("can_see_own_borrowed_books", "See borrowed books"),
                       ("can_borrow_books", "Can borrow books"),
                       ("can_renew", "Can renew a book loan"),
                       )

    def __str__(self):
        """String for representing the Model object."""
        return f'{self.id} ({self.book.title})'

    @property
    def is_overdue(self):
        if self.due_back and date.today() > self.due_back:
            return True
        return False

    @property
    def is_reserved(self):
        if self.reserver:
            return True
        return False

    @property
    def is_renewable(self):
        if self.is_reserved or self.has_been_renewed:
            return False
        return True

    def clean(self):
        super().clean()
        if self.status == 'a' and self.due_back is not None:
            raise ValidationError(message=_('Available books may not have a "due back" date'), code='invalid')
        if self.status not in ('a', 'r') and self.due_back is None:
            raise ValidationError(message=_('Unavailable books must have a "due back" date'), code='invalid')
        if self.borrower and not self.borrower.groups.filter(permissions__codename__contains='can_borrow_books'):
            raise ValidationError(message=_('Only users with borrow permission can borrow books'), code='invalid')
        if self.reserver and not self.reserver.groups.filter(permissions__codename__contains='can_borrow_books'):
            raise ValidationError(message=_('Only users with borrow permission can reserve books'), code='invalid')
        if self.status == 'l' and self.borrower is None:
            raise ValidationError(message=_('Loaned book must be loaned to someone innit?'), code='invalid')
        if self.status != 'l' and self.has_been_renewed:
            raise ValidationError(message=_('Only a loaned book can be renewed'), code='invalid')
        if self.status == 'r' and self.reserver is None:
            raise ValidationError(message=_('A reserved book must have a reserver'), code='invalid')
        if self.borrower and self.reserver and self.borrower == self.reserver:
            raise ValidationError(message=_("The same user can't borrow and reserve a book at the same time"),
                                  code='invalid')
