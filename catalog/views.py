import datetime

from django.contrib import messages
from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.views.generic import CreateView, UpdateView, DeleteView

from catalog.forms import RenewBookModelForm
from catalog.models import Book, BookInstance, Author


def index(request):
    """View function for home page of site."""

    # Generate counts of some of the main objects
    num_books = Book.objects.all().count()
    num_instances = BookInstance.objects.all().count()

    # Available books (status = 'a')
    num_instances_available = BookInstance.objects.filter(status__exact='a').count()

    # The 'all()' is implied by default.
    num_authors = Author.objects.count()

    # Number of visits to this view, as counted in the session variable.
    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1

    context = {
        'num_books': num_books,
        'num_instances': num_instances,
        'num_instances_available': num_instances_available,
        'num_authors': num_authors,
        'num_visits': num_visits,
    }

    # Render the HTML template index.html with the data in the context variable
    return render(request, 'catalog/index.html', context=context)


class BookListView(generic.ListView):
    model = Book
    paginate_by = 10

    def get_queryset(self):
        return Book.objects.order_by('author')


class BookDetailView(generic.DetailView):
    model = Book


class AuthorDetailView(generic.DetailView):
    model = Author


class AuthorListView(generic.ListView):
    model = Author
    context_object_name = 'authors'
    paginate_by = 10

    def get_queryset(self):
        return Author.objects.order_by('last_name')


class BorrowedBooksListView(PermissionRequiredMixin, generic.ListView):
    """
    Generic class-based view listing books on loan to current user.
    Only visible by users who have the "see borrowed books" permission
    which is granted to the 'Library Members' group.
    """
    permission_required = 'catalog.can_see_own_borrowed_books'
    model = BookInstance
    template_name = 'catalog/bookinstance_list_borrowed_user.html'
    paginate_by = 10

    def get_queryset(self):
        return BookInstance.objects.filter(borrower=self.request.user).filter(status__exact='l').order_by('due_back')

    def handle_no_permission(self):
        return big_no_no(view=self, success_redirect_address='my-borrowed', fail_redirect_address='index')


class ReservedBooksListView(PermissionRequiredMixin, generic.ListView):
    """
    Generic class-based view listing books reserved by current user.
    Only visible by users who have the "see borrowed books" permission
    which is granted to the 'Library Members' group.
    """
    permission_required = 'catalog.can_see_own_borrowed_books'
    model = BookInstance
    template_name = 'catalog/bookinstance_list_reserved_user.html'
    paginate_by = 10

    def get_queryset(self):
        return BookInstance.objects.filter(reserver=self.request.user).order_by('due_back')

    def handle_no_permission(self):
        return big_no_no(view=self, success_redirect_address='my-reserved', fail_redirect_address='index')


# https://docs.djangoproject.com/en/3.1/topics/auth/default//#the-permissionrequiredmixin-mixin
# Filtering and Pagination with Django:
# https://www.caktusgroup.com/blog/2018/10/18/filtering-and-pagination-django/
class LoanedBooksListView(PermissionRequiredMixin, generic.ListView):
    """
    Generic class-based view listing books on loan to all users.
    Only visible by users who have the "set book returned" permission,
    which is granted to the 'Librarians' group.
    """
    permission_required = 'catalog.can_mark_returned'
    model = BookInstance
    template_name = 'catalog/bookinstance_list_borrowed_librarians.html'
    paginate_by = 10

    def get_queryset(self):
        return BookInstance.objects.filter(status__exact='l').order_by('borrower', 'due_back')

    def handle_no_permission(self):
        return big_no_no(view=self, success_redirect_address='all-borrowed', fail_redirect_address='index')


def big_no_no(view, success_redirect_address, fail_redirect_address):
    if not view.request.user.is_authenticated:
        # If user is not authenticated, redirect to the login page...
        login_url = reverse('login')
        # ... and then to this one again upon authentication.
        next_url = reverse(success_redirect_address)
        response = ''.join((login_url, '?next=', next_url))
    else:
        permission_denied_message = "ERROR: Permission denied. Your credentials don't allow you to access this page."
        messages.add_message(request=view.request, level=messages.ERROR, message=permission_denied_message)
        response = fail_redirect_address
    return redirect(response, permanent=False)


# replaced by renew_book_with_model_form
# @permission_required('catalog.can_renew', raise_exception=True)
# def renew_book_librarian(request, pk):
#     # We first use the pk argument to get the current BookInstance (if it does exist)
#     book_instance = get_object_or_404(BookInstance, pk=pk)
#
#     # If this is a POST request then process the Form data
#     if request.method == 'POST':
#
#         # Create a form instance and populate it with data from the request (binding):
#         form = RenewBookForm(request.POST)
#
#         # Check if the form is valid:
#         if form.is_valid():
#             # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
#             new_date = form.cleaned_data['renewal_date']
#             if new_date <= book_instance.due_back:
#                 form.add_error('renewal_date', ValidationError(
#                     _('Invalid date - renewal cannot be prior to original due date'),
#                     code='invalid'))
#                 context = {'form': form, 'book_instance': book_instance}
#                 return render(request, 'catalog/book_renew_librarian.html', context)
#
#             elif book_instance.is_overdue and (new_date > book_instance.due_back + datetime.timedelta(weeks=4)):
#                 form.add_error('renewal_date', ValidationError(
#                     _('Invalid date - renewal cannot be more than 4 weeks ahead from original due date'),
#                     code='invalid'))
#                 context = {'form': form, 'book_instance': book_instance}
#                 return render(request, 'catalog/book_renew_librarian.html', context)
#             else:
#                 book_instance.due_back = new_date
#                 book_instance.save()
#
#                 # redirect to a new URL:
#                 return HttpResponseRedirect(reverse('all-borrowed'))
#
#     # If this is a GET (or any other method) create the default form.
#     else:
#         today = datetime.date.today()
#         if book_instance.is_overdue:
#             proposed_renewal_date = book_instance.due_back + datetime.timedelta(weeks=3)
#         else:
#             proposed_renewal_date = today + datetime.timedelta(weeks=3)
#         form = RenewBookForm(initial={'renewal_date': proposed_renewal_date})
#
#     context = {
#         'form': form,
#         'book_instance': book_instance,
#     }
#
#     return render(request, 'catalog/book_renew_librarian.html', context)


class AuthorCreate(PermissionRequiredMixin, CreateView):
    model = Author
    fields = '__all__'
    initial = {'date_of_death': '05/01/2018'}
    permission_required = 'catalog.add_author'

    def handle_no_permission(self):
        return big_no_no(view=self, success_redirect_address='authors', fail_redirect_address='authors')


class AuthorUpdate(PermissionRequiredMixin, UpdateView):
    model = Author
    fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_death']
    permission_required = 'catalog.change_author'

    def handle_no_permission(self):
        return big_no_no(view=self,
                         success_redirect_address='authors',
                         fail_redirect_address='authors')


class AuthorDelete(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Author
    success_url = reverse_lazy('authors')
    permission_required = 'catalog.delete_author'

    def handle_no_permission(self):
        return big_no_no(view=self, success_redirect_address='authors', fail_redirect_address='authors')


@login_required
@permission_required('catalog.can_renew', raise_exception=True, login_url='/catalog/login/')
def renew_book_with_modelform(request, pk):
    book_instance = get_object_or_404(BookInstance, pk=pk)

    # If the book loan can be renewed
    if book_instance.is_renewable:

        # If this is a POST request then process the Form data
        if request.method == 'POST':

            # Create a form instance and populate it with data from the request (binding):
            form = RenewBookModelForm(request.POST, instance=book_instance)

            # Check if the form is valid:
            if form.is_valid():
                # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
                new_date = form.cleaned_data['due_back']
                book_instance.due_back = new_date
                book_instance.has_been_renewed = True
                book_instance.save()
                renewal_success_message = f"The loan of {book_instance.book.title}" \
                                          f"has been renewed to {book_instance.borrower.username} "
                messages.add_message(request=request, level=messages.SUCCESS, message=renewal_success_message)
                # redirect to a new URL:
                return redirect('all-borrowed')

        # If this is a GET (or any other method) create the default form.
        else:
            if book_instance.is_overdue:
                proposed_renewal_date = book_instance.due_back + datetime.timedelta(weeks=3)
            else:
                proposed_renewal_date = datetime.date.today() + datetime.timedelta(weeks=3)

            form = RenewBookModelForm(instance=book_instance, initial={'due_back': proposed_renewal_date})

        context = {
            'form': form,
            'book_instance': book_instance,
        }

        return render(request, 'catalog/book_renew_librarian.html', context)

    else:
        if book_instance.has_been_renewed:
            renewal_denied_message = "ERROR - This book can't be renewed. It has already been renewed once."
        else:
            renewal_denied_message = "ERROR - This book can't be renewed. It's currently reserved by another member."

        messages.add_message(request=request, level=messages.ERROR, message=renewal_denied_message)
        return redirect('all-borrowed')


@login_required
@permission_required('catalog.can_mark_returned', raise_exception=True)
def return_loaned_book(request, pk):
    book_instance = get_object_or_404(BookInstance, pk=pk)

    if book_instance.is_reserved:
        book_instance.status = 'r'
    else:
        book_instance.status = 'a'
    book_instance.due_back = None
    book_instance.borrower = None
    book_instance.has_been_renewed = False
    book_instance.save()
    message = f'The book "{book_instance.book.title}" has been successfully returned'
    messages.add_message(request=request, level=messages.SUCCESS, message=message)
    return redirect(reverse('all-borrowed'))


@login_required
@permission_required('catalog.can_borrow_books', raise_exception=True)
def borrow_book(request, pk):
    bookinst = get_object_or_404(BookInstance, pk=pk)

    if bookinst.is_reserved and bookinst.reserver != request.user:
        borrow_fail_message = f"This copy of {bookinst.book.title} is currently reserved by someone else."
        messages.add_message(request=request, level=messages.ERROR, message=borrow_fail_message)
        return redirect('book-detail', pk=bookinst.book.pk)

    elif bookinst.status == 'l':
        borrow_fail_message = f"This copy of {bookinst.book.title} is already loaned to someone else."
        messages.add_message(request=request, level=messages.ERROR, message=borrow_fail_message)
        return redirect('book-detail', pk=bookinst.book.pk)

    else:
        bookinst.borrower = request.user
        bookinst.status = 'l'
        bookinst.reserver = None
        bookinst.due_back = datetime.date.today() + datetime.timedelta(weeks=3)
        bookinst.save()

        borrow_success_message = f"This copy of {bookinst.book.title} has been added to your borrowed books."
        messages.add_message(request=request, level=messages.SUCCESS, message=borrow_success_message)
        return redirect('my-borrowed')


@login_required
@permission_required('catalog.can_borrow_books', raise_exception=True)
def reserve_book(request, pk):
    bookinst = get_object_or_404(BookInstance, pk=pk)

    if bookinst.status not in ('a', 'l'):
        reservation_fail_message = f"This copy of {bookinst.book.title} can't be reserved at the moment."
        messages.add_message(request=request, level=messages.ERROR, message=reservation_fail_message)
        return redirect('book-detail', pk=bookinst.book.pk)

    else:
        bookinst.reserver = request.user
        if bookinst.status == 'a':
            bookinst.status = 'r'
        bookinst.save()

        reservation_success_message = f"This copy of {bookinst.book.title} has been added to your reservations."
        messages.add_message(request=request, level=messages.SUCCESS, message=reservation_success_message)
        return redirect('my-reserved')


@login_required
@permission_required('catalog.can_borrow_books', raise_exception=True)
def cancel_reservation(request, pk):
    book_instance = get_object_or_404(BookInstance, pk=pk)

    if book_instance.is_reserved:
        if book_instance.reserver == request.user:
            book_instance.reserver = None
            if book_instance.status == "r":
                book_instance.status = 'a'
            book_instance.save()

            cancel_reservation_success_message = f"This copy of {book_instance.book.title} has been removed from your " \
                                                 f"reservations. "
            messages.add_message(request=request, level=messages.SUCCESS, message=cancel_reservation_success_message)
        else:
            cancel_reservation_fail_message = "ERROR - You can't cancel someone else's reservation!"
            messages.add_message(request=request, level=messages.ERROR, message=cancel_reservation_fail_message)
    else:
        cancel_reservation_fail_message = "ERROR - There is no reservation to cancel here!"
        messages.add_message(request=request, level=messages.ERROR, message=cancel_reservation_fail_message)
    return redirect('my-reserved')
