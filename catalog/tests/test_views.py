import datetime
import uuid

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
import catalog.tests.utils as testutils
from catalog.models import Author, BookInstance, Book


class IndexViewTest(TestCase):
    number_of_book_instances = 30
    number_of_books = 15
    number_of_authors = 13
    number_of_visits = 10

    @classmethod
    def setUpTestData(cls):
        testutils.create_a_pannel_of_authors(number=cls.number_of_authors)
        testutils.create_a_pannel_of_books(number=cls.number_of_books)
        testutils.create_a_pannel_of_book_instances(number=cls.number_of_book_instances)

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/catalog/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/index.html')

    def test_displays_number_of_authors(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['num_authors'], self.number_of_authors)

    def test_displays_number_of_books(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['num_books'], self.number_of_books)

    def test_displays_number_of_book_instances(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['num_instances'], self.number_of_book_instances)

    def test_displays_number_of_available_book_instances(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['num_instances_available'], self.number_of_book_instances / 2)

    def test_number_of_visits_displays_zero_the_first_time(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['num_visits'], 0)

    def test_displays_number_of_visits(self):
        response = self.client.get(reverse('index'))
        for i in range(self.number_of_visits):
            response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['num_visits'], self.number_of_visits)


class AuthorListViewTest(TestCase):
    number_of_authors = 13

    @classmethod
    def setUpTestData(cls):
        # Create 13 authors for pagination tests
        testutils.create_a_pannel_of_authors(number=cls.number_of_authors)

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/catalog/authors/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('authors'))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('authors'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/author_list.html')

    def test_pagination_is_ten(self):
        response = self.client.get(reverse('authors'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue(len(response.context['authors']) == 10)

    def test_lists_all_authors(self):
        # Get second page and confirm it has (exactly) remaining 3 items
        response = self.client.get(reverse('authors') + '?page=2')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue(len(response.context['authors']) == 3)


class AuthorDetailViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Author.objects.create(
            first_name=f'Christian',
            last_name=f'Surname',
        )

    def test_view_url_exists_at_desired_location(self):
        author = Author.objects.first()
        response = self.client.get(f'/catalog/author/{author.id}')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        author = Author.objects.first()
        response = self.client.get(reverse('author-detail', kwargs={'pk': author.id}))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        author = Author.objects.first()
        response = self.client.get(reverse('author-detail', kwargs={'pk': author.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/author_detail.html')

    def test_HTTP404_for_invalid_author_id(self):
        author_id = 999
        response = self.client.get(reverse('author-detail', kwargs={'pk': author_id}))
        self.assertEqual(response.status_code, 404)


class BookListViewTest(TestCase):
    number_of_books = 13

    @classmethod
    def setUpTestData(cls):
        # Create 13 authors for pagination tests
        testutils.create_a_pannel_of_books(number=cls.number_of_books)

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/catalog/books/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('books'))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('books'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/book_list.html')

    def test_pagination_is_ten(self):
        response = self.client.get(reverse('books'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue(len(response.context['book_list']) == 10)

    def test_lists_all_books(self):
        # Get second page and confirm it has (exactly) remaining 3 items
        response = self.client.get(reverse('books') + '?page=2')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue(len(response.context['book_list']) == 3)

    def test_list_books_ordered_by_authors(self):
        testutils.create_a_pannel_of_authors(self.number_of_books)
        authors = Author.objects.all()

        books = Book.objects.all()
        for book_id in range(self.number_of_books):
            book = books[book_id]
            book.author = authors[book_id]
            book.save()

        expected_book_list = Book.objects.all().order_by('author')[:10]

        response = self.client.get(reverse('books'))
        actual_book_list = response.context['book_list']

        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertEqual(response.status_code, 200)
        self.assertEqual((actual_book_list[0], actual_book_list[9]), (expected_book_list[0], expected_book_list[9]))


class BookDetailViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Book.objects.create(
            title=f'The Story of the Detail View',
            isbn=f'1234567890123',
            summary=f'This book tells the incredible story behind the Book Detail View.',
            author=Author.objects.create(
                first_name=f'Christian',
                last_name=f'Surname',
            )
        )

    def test_view_url_exists_at_desired_location(self):
        book = Book.objects.first()
        response = self.client.get(f'/catalog/book/{book.id}')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        book = Book.objects.first()
        response = self.client.get(reverse('book-detail', kwargs={'pk': book.id}))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        book = Book.objects.first()
        response = self.client.get(reverse('book-detail', kwargs={'pk': book.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/book_detail.html')

    def test_HTTP404_for_invalid_book_id(self):
        book_id = 999
        response = self.client.get(reverse('book-detail', kwargs={'pk': book_id}))
        self.assertEqual(response.status_code, 404)


class BorrowedBookInstancesByUserListViewTest(TestCase):
    def setUp(self):
        # Create two readers
        reader1: User = testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        reader2: User = testutils.generate_a_library_member(first_name='Michael', last_name='Scott')

        # Create one librarian
        testutils.generate_a_librarian()

        # Create one book
        test_book = testutils.generate_a_book()

        # Create 30 BookInstance objects
        number_of_book_copies = 30
        testutils.create_a_pannel_of_book_instances(number=number_of_book_copies, book=test_book,
                                                    borrowers=(reader1, reader2), reservers=(reader2, reader1))

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('my-borrowed'))
        self.assertRedirects(response, '/accounts/login/?next=/catalog/mybooks/')

    def test_logged_in_uses_correct_template(self):
        user = User.objects.get(first_name='Dwight')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-borrowed'))
        # Check our user is logged in
        self.assertTrue(user.is_authenticated)
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)
        # Check we used correct template
        self.assertTemplateUsed(response, 'catalog/bookinstance_list_borrowed_user.html')

    def test_redirect_if_wrong_permissions(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-borrowed'))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('index'))

    def test_pagination_is_ten(self):
        user = User.objects.get(first_name='Dwight')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-borrowed'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/bookinstance_list_borrowed_user.html')
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue('bookinstance_list' in response.context)
        self.assertEqual(len(response.context['bookinstance_list']), 10)

    def test_lists_all_borrowed_books(self):
        # Get second page and confirm it has (exactly) remaining 5 items
        user = User.objects.get(first_name='Dwight')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-borrowed') + '?page=2')
        borrowed_books = BookInstance.objects.filter(borrower=user).filter(status__exact='l').order_by(
            'due_back').count()
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue('bookinstance_list' in response.context)
        self.assertEqual(len(response.context['bookinstance_list']), borrowed_books - 10)

    def test_lists_only_books_borrowed_by_current_user(self):
        user = User.objects.get(first_name='Dwight')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-borrowed') + '?page=2')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('bookinstance_list' in response.context)
        for bookitem in response.context['bookinstance_list']:
            self.assertEqual(response.context['user'], bookitem.borrower)
            self.assertEqual('l', bookitem.status)

    def test_list_bookinstances_ordered_by_due_date(self):
        user = User.objects.get(first_name='Dwight')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-borrowed') + '?page=2')
        expected_borrowed_books = BookInstance.objects.filter(borrower=user).filter(status__exact='l').order_by(
            'due_back')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('bookinstance_list' in response.context)
        actual_borrowed_book_list = response.context['bookinstance_list']
        for bookitem in actual_borrowed_book_list:
            self.assertEqual(expected_borrowed_books.get(id=bookitem.id), bookitem)


class ReservedBookInstancesByUserListViewTest(TestCase):
    def setUp(self):
        reader1: User = testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        reader2: User = testutils.generate_a_library_member(first_name='Michael', last_name='Scott')
        testutils.generate_a_librarian()
        test_book = testutils.generate_a_book()
        number_of_book_copies = 30
        testutils.create_a_pannel_of_book_instances(number=number_of_book_copies, book=test_book,
                                                    borrowers=(reader1, reader2), reservers=(reader2, reader1))

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('my-reserved'))
        self.assertRedirects(response, '/accounts/login/?next=/catalog/myreservations/')

    def test_logged_in_uses_correct_template(self):
        user = User.objects.get(first_name='Michael')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-reserved'))
        # Check our user is logged in
        self.assertTrue(user.is_authenticated)
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)
        # Check we used correct template
        self.assertTemplateUsed(response, 'catalog/bookinstance_list_reserved_user.html')

    def test_redirect_if_wrong_permissions(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-reserved'))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('index'))

    def test_pagination_is_ten(self):
        user = User.objects.get(first_name='Michael')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-reserved'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/bookinstance_list_reserved_user.html')
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue('bookinstance_list' in response.context)
        self.assertEqual(len(response.context['bookinstance_list']), 10)

    def test_lists_all_reserved_books(self):
        # Get second page and confirm it has (exactly) remaining 5 items
        user = User.objects.get(first_name='Michael')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-reserved') + '?page=2')
        reserved_books = BookInstance.objects.filter(reserver=user).order_by('due_back').count()
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue('bookinstance_list' in response.context)
        self.assertEqual(len(response.context['bookinstance_list']), reserved_books - 10)

    def test_lists_only_books_reserved_by_current_user(self):
        user = User.objects.get(first_name='Michael')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-reserved') + '?page=2')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('bookinstance_list' in response.context)
        for bookitem in response.context['bookinstance_list']:
            self.assertEqual(response.context['user'], bookitem.reserver)

    def test_list_bookinstances_ordered_by_due_date(self):
        user = User.objects.get(first_name='Michael')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('my-reserved') + '?page=2')
        expected_reserved_books = BookInstance.objects.filter(reserver=user).order_by('due_back')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('bookinstance_list' in response.context)
        actual_borrowed_book_list = response.context['bookinstance_list']
        for bookitem in actual_borrowed_book_list:
            self.assertEqual(expected_reserved_books.get(id=bookitem.id), bookitem)


class LoanedBookInstancesByLibrarianListViewTest(TestCase):
    def setUp(self):
        # Create two readers
        reader1: User = testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        reader2: User = testutils.generate_a_library_member(first_name='Michael', last_name='Scott')

        # Create one librarian
        testutils.generate_a_librarian()

        # Create one book
        test_book = testutils.generate_a_book()

        # Create 12 non borrowed BookInstance objects
        number_of_non_borrowed_copies = 12
        testutils.create_a_pannel_of_book_instances(number=number_of_non_borrowed_copies)

        # Create 14 borrowed BookInstance objects
        number_of_borrowed_copies = 14
        testutils.create_a_pannel_of_book_instances(number=number_of_borrowed_copies, book=test_book,
                                                    borrowers=(reader1, reader2), reservers=(reader2, reader1))

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('all-borrowed'))
        self.assertRedirects(response, '/accounts/login/?next=/catalog/borrowedbooks/')

    def test_logged_in_uses_correct_template(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('all-borrowed'))
        # Check our user is logged in
        self.assertTrue(user.is_authenticated)
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)
        # Check we used correct template
        self.assertTemplateUsed(response, 'catalog/bookinstance_list_borrowed_librarians.html')

    def test_redirect_if_logged_in_but_wrong_permissions(self):
        user = User.objects.get(first_name='Dwight')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('all-borrowed'))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('index'))

    def test_pagination_is_ten(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('all-borrowed'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/bookinstance_list_borrowed_librarians.html')
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue('bookinstance_list' in response.context)
        self.assertEqual(len(response.context['bookinstance_list']), 10)

    def test_lists_all_borrowed_books(self):
        # Get second page and confirm it has (exactly) remaining 4 items
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('all-borrowed') + '?page=2')
        loaned_books = BookInstance.objects.filter(status__exact='l').order_by('borrower', 'due_back').count()
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue('bookinstance_list' in response.context)
        self.assertEqual(len(response.context['bookinstance_list']), loaned_books - 10)

    def test_list_bookinstances_ordered_by_borrower_then_due_date(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('all-borrowed') + '?page=2')
        expected_borrowed_books = BookInstance.objects.filter(status__exact='l').order_by('borrower', 'due_back')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('bookinstance_list' in response.context)
        actual_borrowed_book_list = response.context['bookinstance_list']
        for bookitem in actual_borrowed_book_list:
            self.assertEqual(expected_borrowed_books.get(id=bookitem.id), bookitem)


class RenewBookInstancesViewTest(TestCase):
    bookinst_id = 'f4d83f99-dc82-4ce3-bcb7-737c9e4761e9'

    def setUp(self):
        reader: User = testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        testutils.generate_a_librarian()
        test_book = testutils.generate_a_book()
        testutils.create_bookinstance_object(bookinstance_id=self.bookinst_id, status='l', book=test_book,
                                             borrower=reader, imprint='Paperback',
                                             due_back=datetime.date.today() + datetime.timedelta(days=5))

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}))
        # Manually check redirect (Can't use assertRedirect, because the redirect URL is unpredictable)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/accounts/login/'))

    def test_logged_in_with_permission_to_renew_book(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 200)

    def test_HTTP403_if_logged_in_but_not_correct_permission(self):
        user = User.objects.get(username='d_schrute')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 403)

    def test_HTTP404_for_invalid_book_if_logged_in(self):
        # unlikely UID to match our bookinstance!
        test_uid = uuid.uuid4()
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('renew-book-librarian', kwargs={'pk': test_uid}))
        self.assertEqual(response.status_code, 404)

    def test_uses_correct_template(self):
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/book_renew_librarian.html')

    def test_form_renewal_date_initially_has_date_three_weeks_in_future(self):
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}))
        self.assertEqual(response.status_code, 200)
        date_3_weeks_in_future = datetime.date.today() + datetime.timedelta(weeks=3)
        self.assertEqual(response.context['form'].initial['due_back'], date_3_weeks_in_future)

    def test_form_renewal_date_initially_has_date_three_weeks_after_overdue_return_date(self):
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        bookinst.due_back = yesterday
        bookinst.save()
        response = self.client.get(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}))
        self.assertEqual(response.status_code, 200)
        date_3_weeks_after_overdue_date = yesterday + datetime.timedelta(weeks=3)
        self.assertTrue(response.context['form'].instance.is_overdue)
        self.assertEqual(response.context['form'].initial['due_back'], date_3_weeks_after_overdue_date)

    def test_redirects_to_all_borrowed_book_list_on_success(self):
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        valid_date_in_future = datetime.date.today() + datetime.timedelta(weeks=4)
        response = self.client.post(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id, }),
                                    {'due_back': valid_date_in_future})
        self.assertRedirects(response, reverse('all-borrowed'))

    def test_form_invalid_renewal_date_past(self):
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        date_in_past = datetime.date.today() - datetime.timedelta(weeks=1)
        response = self.client.post(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}),
                                    {'due_back': date_in_past})
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'due_back', 'Invalid date - renewal in past')

    def test_form_invalid_renewal_date_before_original_due_date(self):
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        bookinst.due_back = datetime.date.today() + datetime.timedelta(weeks=3)
        bookinst.save()
        invalid_date = datetime.date.today() + datetime.timedelta(weeks=2)
        response = self.client.post(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}),
                                    {'due_back': invalid_date})
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'due_back',
                             'Invalid date - renewal before original due date')

    def test_form_invalid_renewal_date_too_far_in_future(self):
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        invalid_date_in_future = datetime.date.today() + datetime.timedelta(weeks=5)
        response = self.client.post(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}),
                                    {'due_back': invalid_date_in_future})
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'due_back', 'Invalid date - renewal more than 4 weeks ahead from now')

    def test_form_invalid_renewal_date_future_for_overdue_book(self):
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        bookinst.due_back = datetime.date.today() - datetime.timedelta(days=1)
        bookinst.save()
        invalid_date_in_future = datetime.date.today() + datetime.timedelta(weeks=4)
        response = self.client.post(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}),
                                    {'due_back': invalid_date_in_future})
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['form'].instance.is_overdue)
        self.assertFormError(response, 'form', 'due_back',
                             'Invalid date - renewal more than 4 weeks ahead from original due date')

    def test_invalid_renewal_of_book_already_renewed_redirects_to_all_borrowed(self):
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        bookinst.has_been_renewed = True
        bookinst.save()
        valid_date_in_future = datetime.date.today() + datetime.timedelta(weeks=4)
        response = self.client.post(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}),
                                    {'due_back': valid_date_in_future})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('all-borrowed'))

    def test_invalid_renewal_of_reserved_book_redirects_to_all_borrowed(self):
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        reserver: User = testutils.generate_a_library_member(first_name='Michael', last_name='Scott')
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        bookinst.reserver = reserver
        bookinst.save()
        valid_date_in_future = datetime.date.today() + datetime.timedelta(weeks=4)
        response = self.client.post(reverse('renew-book-librarian', kwargs={'pk': self.bookinst_id}),
                                    {'due_back': valid_date_in_future})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('all-borrowed'))


class ReturnLoanedBookViewTest(TestCase):
    bookinst_id = 'f4d83f99-dc82-4ce3-bcb7-737c9e4761e9'

    def setUp(self):
        reader: User = testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        testutils.generate_a_librarian()
        test_book = testutils.generate_a_book()
        testutils.create_bookinstance_object(bookinstance_id=self.bookinst_id, status='l', book=test_book,
                                             borrower=reader, imprint='Paperback',
                                             due_back=datetime.date.today() + datetime.timedelta(days=5))

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('return-book', kwargs={'pk': self.bookinst_id}))
        # Manually check redirect (Can't use assertRedirect, because the redirect URL is unpredictable)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/accounts/login/'))

    def test_logged_in_with_permission_can_return_book(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        response = self.client.get(reverse('return-book', kwargs={'pk': self.bookinst_id}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('all-borrowed'))

    def test_HTTP403_if_logged_in_but_not_correct_permission(self):
        user = User.objects.get(username='d_schrute')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('return-book', kwargs={'pk': self.bookinst_id}))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 403)

    def test_HTTP404_for_invalid_book_if_logged_in(self):
        test_uid = uuid.uuid4()
        self.client.login(username='librarian', password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('return-book', kwargs={'pk': test_uid}))
        self.assertEqual(response.status_code, 404)

    def test_return_non_reserved_book_marks_it_available(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        # Check if book appears in the borrowed books list
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        response_after_return = self.client.get(reverse('all-borrowed'))
        self.assertIn(bookinst, response_after_return.context['bookinstance_list'])
        # Return the book
        response = self.client.get(reverse('return-book', kwargs={'pk': self.bookinst_id}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('all-borrowed'))
        # Check status, borrower, due_back, has_been_renewed of returned book
        bookinst.refresh_from_db()
        self.assertEqual((bookinst.status, bookinst.borrower, bookinst.due_back, bookinst.has_been_renewed),
                         ('a', None, None, False))
        # Check if returned book has been removed from all borrowed
        response_after_return = self.client.get(reverse('all-borrowed'))
        self.assertNotIn(bookinst, response_after_return.context['bookinstance_list'])

    def test_return_reserved_book_marks_it_reserved(self):
        # Log the librarian in
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        # Add a reserver to the book
        reserver: User = testutils.generate_a_library_member(first_name='Michael', last_name='Scott')
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        bookinst.reserver = reserver
        bookinst.save()
        # Check if book appears in the borrowed books list
        response_after_return = self.client.get(reverse('all-borrowed'))
        self.assertIn(bookinst, response_after_return.context['bookinstance_list'])
        # Return the book
        response = self.client.get(reverse('return-book', kwargs={'pk': self.bookinst_id}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('all-borrowed'))
        # Check status, borrower, due_back, has_been_renewed and reserver of returned book
        bookinst.refresh_from_db()
        self.assertEqual(
            (bookinst.status, bookinst.borrower, bookinst.due_back, bookinst.has_been_renewed, bookinst.reserver),
            ('r', None, None, False, reserver))
        # Check if returned book has been removed from all borrowed
        response_after_return = self.client.get(reverse('all-borrowed'))
        self.assertNotIn(bookinst, response_after_return.context['bookinstance_list'])


class BorrowBookViewTest(TestCase):
    bookinst_id = 'f4d83f99-dc82-4ce3-bcb7-737c9e4761e9'

    def setUp(self):
        testutils.generate_a_library_member(first_name='Michael', last_name='Scott')
        testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        testutils.generate_a_librarian()
        test_book = testutils.generate_a_book()
        testutils.create_bookinstance_object(bookinstance_id=self.bookinst_id, status='a', book=test_book,
                                             imprint='Paperback')

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('borrow-book', kwargs={'pk': self.bookinst_id}))
        # Manually check redirect (Can't use assertRedirect, because the redirect URL is unpredictable)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/accounts/login/'))

    def test_logged_in_with_permission_can_borrow_book(self):
        user = User.objects.get(username='m_scott')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        response = self.client.get(reverse('borrow-book', kwargs={'pk': self.bookinst_id}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('my-borrowed'))

    def test_HTTP403_if_logged_in_but_not_correct_permission(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('borrow-book', kwargs={'pk': self.bookinst_id}))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 403)

    def test_HTTP404_for_invalid_book_if_logged_in(self):
        test_uid = uuid.uuid4()
        self.client.login(username='m_scott', password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('borrow-book', kwargs={'pk': test_uid}))
        self.assertEqual(response.status_code, 404)

    def test_borrow_of_book_reserved_by_another_invalid_and_redirects_to_book_detail(self):
        # Log user in
        user = User.objects.get(username='m_scott')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        # Add a reserver to the book
        reserver = User.objects.get(username='d_schrute')
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        bookinst.reserver = reserver
        bookinst.status = 'r'
        bookinst.save()
        # Try to borrow the book
        response = self.client.get(reverse('borrow-book', kwargs={'pk': self.bookinst_id}))
        # Check status, borrower, due_back, has_been_renewed and reserver haven't been changed
        bookinst.refresh_from_db()
        self.assertEqual(
            (bookinst.status, bookinst.borrower, bookinst.due_back, bookinst.has_been_renewed, bookinst.reserver),
            ('r', None, None, False, reserver))
        # Check if it redirected to book-detail
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('book-detail', kwargs={'pk': bookinst.book.id}))

    def test_borrow_of_book_borrowed_by_another_invalid_and_redirects_to_book_detail(self):
        # Log the user in
        user = User.objects.get(username='m_scott')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        # Add a borrower to the book
        borrower = User.objects.get(username='d_schrute')
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        bookinst.borrower = borrower
        bookinst.status = 'l'
        bookinst.due_back = datetime.date.today()
        bookinst.save()
        # Try to borrow the book
        response = self.client.get(reverse('borrow-book', kwargs={'pk': self.bookinst_id}))
        # Check status, borrower, due_back, has_been_renewed and borrower haven't been changed
        bookinst.refresh_from_db()
        self.assertEqual(bookinst.borrower, borrower)
        # Check if it redirected to book-detail
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('book-detail', kwargs={'pk': bookinst.book.id}))

    def test_borrow_book_success_redirects_to_my_borrowed(self):
        # Log user in
        user = User.objects.get(username='m_scott')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        my_borrowed_first_response = self.client.get(reverse('my-borrowed'))
        self.assertNotIn(bookinst, my_borrowed_first_response.context['bookinstance_list'])
        # Try to borrow the book
        response = self.client.get(reverse('borrow-book', kwargs={'pk': self.bookinst_id}))
        # Check borrower of borrowed book is the user
        bookinst.refresh_from_db()
        self.assertEqual(bookinst.borrower, user)
        # Check if it redirected to my-borrowed
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('my-borrowed'))
        my_borrowed_second_response = self.client.get(reverse('my-borrowed'))
        self.assertIn(bookinst, my_borrowed_second_response.context['bookinstance_list'])


class ReserveBookViewTest(TestCase):
    bookinst_id = 'f4d83f99-dc82-4ce3-bcb7-737c9e4761e9'

    def setUp(self):
        testutils.generate_a_library_member(first_name='Michael', last_name='Scott')
        testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        testutils.generate_a_librarian()
        test_book = testutils.generate_a_book()
        testutils.create_bookinstance_object(bookinstance_id=self.bookinst_id, status='a', book=test_book,
                                             imprint='Paperback')

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('reserve-book', kwargs={'pk': self.bookinst_id}))
        # Manually check redirect (Can't use assertRedirect, because the redirect URL is unpredictable)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/accounts/login/'))

    def test_logged_in_with_permission_can_borrow_book(self):
        user = User.objects.get(username='m_scott')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        response = self.client.get(reverse('reserve-book', kwargs={'pk': self.bookinst_id}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('my-reserved'))

    def test_HTTP403_if_logged_in_but_not_correct_permission(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('reserve-book', kwargs={'pk': self.bookinst_id}))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 403)

    def test_HTTP404_for_invalid_book_if_logged_in(self):
        test_uid = uuid.uuid4()
        self.client.login(username='m_scott', password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('reserve-book', kwargs={'pk': test_uid}))
        self.assertEqual(response.status_code, 404)

    def test_reservation_of_book_reserved_by_another_invalid_and_redirects_to_book_detail(self):
        # Log user in
        user = User.objects.get(username='m_scott')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        # Add a reserver to the book
        reserver = User.objects.get(username='d_schrute')
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        bookinst.reserver = reserver
        bookinst.status = 'r'
        bookinst.save()
        # Try to reserve the book
        response = self.client.get(reverse('borrow-book', kwargs={'pk': self.bookinst_id}))
        # Check status and reserver haven't been changed
        bookinst.refresh_from_db()
        self.assertEqual((bookinst.status, bookinst.reserver), ('r', reserver))
        # Check if it redirected to book-detail
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('book-detail', kwargs={'pk': bookinst.book.id}))

    def test_reservation_of_book_under_maintenance_invalid_and_redirects_to_book_detail(self):
        # Log user in
        user = User.objects.get(username='m_scott')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        # Add a reserver to the book
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        bookinst.status = 'm'
        bookinst.due_back = datetime.date.today()
        bookinst.save()
        # Try to reserve the book
        response = self.client.get(reverse('reserve-book', kwargs={'pk': self.bookinst_id}))
        # Check status and reserver haven't been changed
        bookinst.refresh_from_db()
        self.assertEqual((bookinst.status, bookinst.reserver), ('m', None))
        # Check if it redirected to book-detail
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('book-detail', kwargs={'pk': bookinst.book.id}))

    def test_reservation_success_redirects_to_my_reserved(self):
        # Log user in
        user = User.objects.get(username='m_scott')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        # Check that book is not in my reservations
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        reservations_first_response = self.client.get(reverse('my-reserved'))
        self.assertNotIn(bookinst, reservations_first_response.context['bookinstance_list'])
        # Try to reserve the book
        response = self.client.get(reverse('reserve-book', kwargs={'pk': self.bookinst_id}))
        # Check status and reserver have been changed
        bookinst.refresh_from_db()
        self.assertEqual((bookinst.status, bookinst.reserver), ('r', user))
        # Check if it redirected to book-detail
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('my-reserved'))
        # Check if book has been added to my reservations
        reservations_second_response = self.client.get(reverse('my-reserved'))
        self.assertIn(bookinst, reservations_second_response.context['bookinstance_list'])


class CancelReservationViewTest(TestCase):
    bookinst_id = 'f4d83f99-dc82-4ce3-bcb7-737c9e4761e9'

    def setUp(self):
        reader1 = testutils.generate_a_library_member(first_name='Michael', last_name='Scott')
        testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        testutils.generate_a_librarian()
        test_book = testutils.generate_a_book()
        testutils.create_bookinstance_object(bookinstance_id=self.bookinst_id, status='r', book=test_book,
                                             imprint='Paperback', reserver=reader1)

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('cancel-res', kwargs={'pk': self.bookinst_id}))
        # Manually check redirect (Can't use assertRedirect, because the redirect URL is unpredictable)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/accounts/login/'))

    def test_logged_in_with_permission_can_borrow_book(self):
        user = User.objects.get(username='m_scott')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        response = self.client.get(reverse('cancel-res', kwargs={'pk': self.bookinst_id}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('my-reserved'))

    def test_HTTP403_if_logged_in_but_not_correct_permission(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('cancel-res', kwargs={'pk': self.bookinst_id}))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 403)

    def test_HTTP404_for_invalid_book_if_logged_in(self):
        test_uid = uuid.uuid4()
        self.client.login(username='m_scott', password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('cancel-res', kwargs={'pk': test_uid}))
        self.assertEqual(response.status_code, 404)

    def test_cancel_reservation_success_redirects_to_my_reserved(self):
        # Log user in
        user = User.objects.get(username='m_scott')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        # Check that book is in my reservations
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        reservations_first_response = self.client.get(reverse('my-reserved'))
        self.assertIn(bookinst, reservations_first_response.context['bookinstance_list'])
        # Try to cancel reservation of the book
        response = self.client.get(reverse('cancel-res', kwargs={'pk': self.bookinst_id}))
        # Check status and reserver have been changed
        bookinst.refresh_from_db()
        self.assertEqual((bookinst.status, bookinst.reserver), ('a', None))
        # Check if it redirected to my reservations
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('my-reserved'))
        # Check if book has been removed from my reservations
        reservations_second_response = self.client.get(reverse('my-reserved'))
        self.assertNotIn(bookinst, reservations_second_response.context['bookinstance_list'])

    def test_cancel_someone_elses_reservation_is_invalid(self):
        # Log user in
        user = User.objects.get(username='d_schrute')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        # Try to cancel reservation of the book
        response = self.client.get(reverse('cancel-res', kwargs={'pk': self.bookinst_id}))
        # Check reserver hasn't been changed
        bookinst.refresh_from_db()
        self.assertNotEqual(bookinst.reserver, user)
        # Check if it redirected to my reservations
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('my-reserved'))

    def test_cancel_reservation_that_doesnt_exist_redirects_to_my_borrowed(self):
        # Log user in
        user = User.objects.get(username='d_schrute')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        bookinst = BookInstance.objects.get(id=self.bookinst_id)
        bookinst.reserver = None
        bookinst.status = 'a'
        bookinst.save()
        # Try to cancel reservation of the book
        response = self.client.get(reverse('cancel-res', kwargs={'pk': self.bookinst_id}))
        # Check if it redirected to my reservations
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('my-reserved'))


class AuthorCreateViewTest(TestCase):
    def setUp(self):
        testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        testutils.generate_a_librarian()

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('author-create'))
        # Manually check redirect (Can't use assertRedirect, because the redirect URL is unpredictable)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/accounts/login/'))

    def test_redirect_if_logged_in_but_not_correct_permission(self):
        user = User.objects.get(username='d_schrute')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('author-create'))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('authors'))

    def test_logged_in_with_permission_uses_correct_template(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        response = self.client.get(reverse('author-create'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('catalog.author_form.html')

    def test_author_create_form_displays_correct_fields(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        response = self.client.get(reverse('author-create'))
        self.assertEqual(response.status_code, 200)
        expected_fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_death']
        form = response.context['form']
        for name in form.fields.keys():
            self.assertIn(name, expected_fields)

    def test_author_create_form_displays_correct_initial(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        response = self.client.get(reverse('author-create'))
        self.assertEqual(response.status_code, 200)
        expected_initial = {'date_of_death': '05/01/2018'}
        actual_initial = response.context['form'].initial
        self.assertEqual(actual_initial, expected_initial)


class AuthorUpdateViewTest(TestCase):
    author_id = 1
    def setUp(self):
        testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        testutils.generate_a_librarian()
        testutils.create_author_object(author_id=self.author_id, first_name='Billy', last_name='Bob')

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('author-update', kwargs={'pk': self.author_id}))
        # Manually check redirect (Can't use assertRedirect, because the redirect URL is unpredictable)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/accounts/login/'))

    def test_redirect_if_logged_in_but_not_correct_permission(self):
        user = User.objects.get(username='d_schrute')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('author-update', kwargs={'pk': self.author_id}))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('authors'))

    def test_logged_in_with_permission_uses_correct_template(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        response = self.client.get(reverse('author-update', kwargs={'pk': self.author_id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('catalog.author_form.html')

    def test_HTTP404_for_invalid_book_if_logged_in(self):
        invalid_id = 999
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('author-update', kwargs={'pk': invalid_id}))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 404)

    def test_author_update_form_displays_correct_fields(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        response = self.client.get(reverse('author-update', kwargs={'pk': self.author_id}))
        self.assertEqual(response.status_code, 200)
        expected_fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_death']
        form = response.context['form']
        for name in form.fields.keys():
            self.assertIn(name, expected_fields)

class AuthorDeleteViewTest(TestCase):
    author_id = 1
    def setUp(self):
        testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        testutils.generate_a_librarian()
        testutils.create_author_object(author_id=self.author_id, first_name='Billy', last_name='Bob')

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('author-delete', kwargs={'pk': self.author_id}))
        # Manually check redirect (Can't use assertRedirect, because the redirect URL is unpredictable)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/accounts/login/'))

    def test_redirect_if_logged_in_but_not_correct_permission(self):
        user = User.objects.get(username='d_schrute')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('author-delete', kwargs={'pk': self.author_id}))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('authors'))

    def test_HTTP404_for_invalid_book_if_logged_in(self):
        invalid_id = 999
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        response = self.client.get(reverse('author-delete', kwargs={'pk': invalid_id}))
        self.assertTrue(user.is_authenticated)
        self.assertEqual(response.status_code, 404)

    def test_logged_in_with_permission_uses_correct_template(self):
        user = User.objects.get(username='librarian')
        self.client.login(username=user.username, password='SuperSTR0ngP4ssW0rd!!')
        self.assertTrue(user.is_authenticated)
        response = self.client.get(reverse('author-delete', kwargs={'pk': self.author_id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('catalog.author_confirm_delete.html')
