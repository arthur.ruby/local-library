import datetime
import uuid

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils.translation import gettext_lazy as _
import catalog.tests.utils as testutils
from catalog.models import Author, Language, Genre, Book, BookInstance


# https://coverage.readthedocs.io/en/latest/
# https://adamj.eu/tech/2019/04/30/getting-a-django-application-to-100-percent-coverage/
# Remplacer commande "python" par "coverage run"
# Pour définir la portée du rapport: "--source=." pour tout
#                                    "--source=catalog" pour l'app "catalog"
# coverage run --source=catalog manage.py test catalog.tests.tests_model
# coverage report


class BookModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        testutils.generate_a_book()

    # Title field
    def test_title_label(self):
        book = Book.objects.get(id=1)
        field_label = book._meta.get_field('title').verbose_name
        self.assertEqual(field_label, 'title')

    def test_title_max_length(self):
        book = Book.objects.get(id=1)
        max_length = book._meta.get_field('title').max_length
        self.assertEqual(max_length, 200)

    def test_title_help_text(self):
        book = Book.objects.get(id=1)
        help_text = book._meta.get_field('title').help_text
        self.assertEqual(help_text, 'Enter the book title (e.g. Lonesome Dove)')

    # ISBN field
    def test_isbn_label(self):
        book = Book.objects.get(id=1)
        field_label = book._meta.get_field('isbn').verbose_name
        self.assertEqual(field_label, 'ISBN')

    def test_isbn_max_length(self):
        book = Book.objects.get(id=1)
        max_length = book._meta.get_field('isbn').max_length
        self.assertEqual(max_length, 13)

    def test_isbn_help_text(self):
        book = Book.objects.get(id=1)
        help_text = book._meta.get_field('isbn').help_text
        self.assertEqual(help_text,
                         '13 Character <a href="https://www.isbn-international.org/content/what-isbn">ISBN number</a>')

    # Summary field
    def test_summary_label(self):
        book = Book.objects.get(id=1)
        field_label = book._meta.get_field('summary').verbose_name
        self.assertEqual(field_label, 'summary')

    def test_summary_max_length(self):
        book = Book.objects.get(id=1)
        max_length = book._meta.get_field('summary').max_length
        self.assertEqual(max_length, 1000)

    def test_summary_help_text(self):
        book = Book.objects.get(id=1)
        help_text = book._meta.get_field('summary').help_text
        self.assertEqual(help_text, 'Enter a brief description of the book')

    # Author field
    def test_author_relation_type(self):
        book = Book.objects.get(id=1)
        self.assertIsInstance(book.author, Author)

    def test_author_fk_relation_exists(self):
        author = Author.objects.get(id=1)
        relation_exists = Book.objects.filter(author__exact=author).exists()
        self.assertTrue(relation_exists)

    def test_author_field_is_nullable(self):
        book = Book.objects.get(id=1)
        is_nullable = book._meta.get_field('author').null
        self.assertTrue(is_nullable)

    def test_author_field_on_delete_is_set_null(self):
        book = Book.objects.get(id=1)
        book.author.delete()
        book.refresh_from_db()
        self.assertIsNone(book.author)

    # Genre field
    def test_genre_relation_type(self):
        book = Book.objects.get(id=1)
        self.assertIsInstance(book.genre.first(), Genre)

    def test_genre_fk_relation_exists(self):
        genre = Genre.objects.first()
        relation_exists = Book.objects.filter(genre__exact=genre).exists()
        self.assertTrue(relation_exists)

    def test_genre_help_text(self):
        book = Book.objects.get(id=1)
        help_text = book._meta.get_field('genre').help_text
        self.assertEqual(help_text, 'Select a genre for this book (e.g. Science Fiction)')

    # Language field
    def test_language_relation_type(self):
        book = Book.objects.get(id=1)
        self.assertIsInstance(book.language, Language)

    def test_language_fk_relation_exists(self):
        language = Language.objects.get(id=1)
        relation_exists = Book.objects.filter(language__exact=language).exists()
        self.assertTrue(relation_exists)

    def test_language_field_is_nullable(self):
        book = Book.objects.get(id=1)
        is_nullable = book._meta.get_field('language').null
        self.assertTrue(is_nullable)

    def test_language_field_on_delete_is_set_null(self):
        book = Book.objects.get(id=1)
        book.language.delete()
        book.refresh_from_db()
        self.assertIsNone(book.language)

    # Model Methods
    def test_object_name_is_the_title_field(self):
        book = Book.objects.get(id=1)
        expected_object_name = f'{book.title}'
        self.assertEqual(expected_object_name, str(book))

    def test_get_absolute_url(self):
        book = Book.objects.get(id=1)
        self.assertEqual(book.get_absolute_url(), '/catalog/book/1')

    def test_display_genre_is_genre_names_separated_by_commas(self):
        book = Book.objects.get(id=1)
        expected_display_genre = ", ".join(str(value['name']) for value in book.genre.all().values())
        self.assertEqual(book.display_genre(), expected_display_genre)

    def test_display_genre_short_description_is_genre_model_name(self):
        book = Book.objects.get(id=1)
        genre_name = Genre.__name__
        self.assertEqual(book.display_genre.short_description, genre_name)


class AuthorModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        testutils.create_author_object(author_id=1, first_name='Big', last_name='Bob')

    # First name field
    def test_first_name_label(self):
        author = Author.objects.get(id=1)
        field_label = author._meta.get_field('first_name').verbose_name
        self.assertEqual(field_label, 'first name')

    def test_first_name_max_length(self):
        author = Author.objects.get(id=1)
        max_length = author._meta.get_field('first_name').max_length
        self.assertEqual(max_length, 100)

    # Last name field
    def test_last_name_label(self):
        author = Author.objects.get(id=1)
        field_label = author._meta.get_field('last_name').verbose_name
        self.assertEqual(field_label, 'last name')

    def test_last_name_max_length(self):
        author = Author.objects.get(id=1)
        max_length = author._meta.get_field('last_name').max_length
        self.assertEqual(max_length, 100)

    # Date of birth field
    def test_date_of_birth_label(self):
        author = Author.objects.get(id=1)
        field_label = author._meta.get_field('date_of_birth').verbose_name
        self.assertEqual(field_label, 'Born')

    def test_date_of_birth_field_is_nullable(self):
        author = Author.objects.get(id=1)
        is_nullable = author._meta.get_field('date_of_birth').null
        self.assertTrue(is_nullable)

    def test_date_of_birth_field_can_be_blank(self):
        author = Author.objects.get(id=1)
        can_be_blank = author._meta.get_field('date_of_birth').blank
        self.assertTrue(can_be_blank)

    # Date of death field
    def test_date_of_death_label(self):
        author = Author.objects.get(id=1)
        field_label = author._meta.get_field('date_of_death').verbose_name
        self.assertEqual(field_label, 'Died')

    def test_date_of_death_field_is_nullable(self):
        author = Author.objects.get(id=1)
        is_nullable = author._meta.get_field('date_of_death').null
        self.assertTrue(is_nullable)

    def test_date_of_death_field_can_be_blank(self):
        author = Author.objects.get(id=1)
        can_be_blank = author._meta.get_field('date_of_death').blank
        self.assertTrue(can_be_blank)

    # Model validation
    def test_death_in_future_raises_validation_exception(self):
        author = Author.objects.get(id=1)
        author.date_of_death = datetime.date.today() + datetime.timedelta(days=1)
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Date of Death cannot be in the future'))

        with self.assertRaises(expected_exception=ValidationError):
            author.clean()

    def test_birth_in_future_raises_validation_exception(self):
        author = Author.objects.get(id=1)
        author.date_of_birth = datetime.date.today() + datetime.timedelta(days=1)
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('ate of Birth cannot be in the future'))

        with self.assertRaises(expected_exception=ValidationError):
            author.clean()

    def test_death_before_birth_raises_validation_exception(self):
        author = Author.objects.get(id=1)
        author.date_of_death = '1950-12-25'
        author.date_of_birth = '2000-11-30'
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Date of birth cannot be after date of Death'))

        with self.assertRaises(expected_exception=ValidationError):
            author.clean()

    # __str__ method
    def test_object_name_is_last_name_comma_first_name(self):
        author = Author.objects.get(id=1)
        expected_object_name = f'{author.last_name}, {author.first_name}'
        self.assertEqual(expected_object_name, str(author))

    # get_absolute_url() method
    def test_get_absolute_url(self):
        author = Author.objects.get(id=1)
        # This will also fail if the urlconf is not defined.
        self.assertEqual(author.get_absolute_url(), '/catalog/author/1')

    # Meta
    def test_ordering_by_due_back(self):
        author = Author.objects.get(id=1)
        ordering = author._meta.ordering
        expected_ordering = ['last_name', 'first_name']
        self.assertEqual(ordering, expected_ordering)


class LanguageModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        testutils.create_language_object(language_id=1, name='English')

    # Name field
    def test_name_label(self):
        language = Language.objects.get(id=1)
        field_label = language._meta.get_field('name').verbose_name
        self.assertEqual(field_label, 'name')

    def test_name_max_length(self):
        language = Language.objects.get(id=1)
        max_length = language._meta.get_field('name').max_length
        self.assertEqual(max_length, 200)

    def test_name_help_text(self):
        language = Language.objects.get(id=1)
        help_text = language._meta.get_field('name').help_text
        self.assertEqual(help_text, "Enter the book's natural language (e.g. English, French, Japanese etc.)")

    # __str__ method
    def test_object_name_is_the_name_field(self):
        language = Language.objects.get(id=1)
        expected_object_name = f'{language.name}'
        self.assertEqual(expected_object_name, str(language))


class GenreModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Genre.objects.get_or_create(id=1, name='Young Adult')

    # Name field
    def test_name_label(self):
        genre = Genre.objects.get(id=1)
        field_label = genre._meta.get_field('name').verbose_name
        self.assertEqual(field_label, 'name')

    def test_name_max_length(self):
        genre = Genre.objects.get(id=1)
        max_length = genre._meta.get_field('name').max_length
        self.assertEqual(max_length, 200)

    def test_name_help_text(self):
        genre = Genre.objects.get(id=1)
        help_text = genre._meta.get_field('name').help_text
        self.assertEqual(help_text, 'Enter a book genre (e.g. Science Fiction)')

    # Relation many to many books
    def test_genre_bookset_relation_type(self):
        genre = Genre.objects.get(id=1)
        book = testutils.generate_a_book()
        genre.book_set.add(book)
        self.assertIsInstance(genre.book_set.first(), Book)

    def test_book_fk_relation_exists(self):
        genre = Genre.objects.get(id=1)
        book = testutils.generate_a_book()
        genre.book_set.add(book)
        relation_exists = Genre.objects.filter(book__exact=book).exists()
        self.assertTrue(relation_exists)

    # __str__ method
    def test_object_name_is_the_name_field(self):
        genre = Genre.objects.get(id=1)
        expected_object_name = f'{genre.name}'
        self.assertEqual(expected_object_name, str(genre))


class BookInstanceModelTest(TestCase):
    bookinst_id = 'f4d83f99-dc82-4ce3-bcb7-737c9e4761e9'

    @classmethod
    def setUpTestData(cls):
        book = testutils.generate_a_book()

        testutils.generate_a_librarian()
        reader1 = testutils.generate_a_library_member(first_name='Dwight', last_name='Schrute')
        reader2 = testutils.generate_a_library_member(first_name='Michael', last_name='Scott')

        testutils.create_bookinstance_object(bookinstance_id=cls.bookinst_id, book=book, imprint='Paperback',
                                             status='l',
                                             borrower=reader1, reserver=reader2,
                                             due_back=datetime.date.today() + datetime.timedelta(
                                                 weeks=3))

    # UUID field
    def test_uuid_help_text(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        help_text = book_instance._meta.get_field('id').help_text
        self.assertEqual(help_text, 'Unique ID for this particular book across whole library')

    def test_uuid_default_is_uuid4(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        default = book_instance._meta.get_field('id').default
        self.assertIs(default, uuid.uuid4)

    def test_uuid_field_is_pk(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        pk = book_instance._meta.get_field('id').primary_key
        self.assertTrue(pk)

    # Book field
    def test_book_field_is_nullable(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        is_nullable = book_instance._meta.get_field('book').null
        self.assertTrue(is_nullable)

    def test_book_relation_type(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        self.assertIsInstance(book_instance.book, Book)

    def test_book_fk_relation_exists(self):
        book = Book.objects.get(id=1)
        relation_exists = BookInstance.objects.filter(book__exact=book).exists()
        self.assertTrue(relation_exists)

    def test_book_field_on_delete_is_set_null(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.book.delete()
        book_instance.refresh_from_db()
        self.assertIsNone(book_instance.book)

    # Imprint field
    def test_imprint_max_length(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        max_length = book_instance._meta.get_field('imprint').max_length
        self.assertEqual(max_length, 200)

    # Status field
    def test_status_help_text(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        help_text = book_instance._meta.get_field('status').help_text
        self.assertEqual(help_text, 'Book availability')

    def test_status_max_length(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        max_length = book_instance._meta.get_field('status').max_length
        self.assertEqual(max_length, 1)

    def test_status_can_be_blank(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        can_be_blank = book_instance._meta.get_field('status').blank
        self.assertTrue(can_be_blank)

    def test_status_default_is_m(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        default = book_instance._meta.get_field('status').default
        self.assertEqual(default, 'm')

    def test_status_choices_is_m_l_a_r_enum(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        expected_loan_status = (('m', 'Maintenance'),
                                ('l', 'Loaned'),
                                ('a', 'Available'),
                                ('r', 'Reserved'),)

        choices = book_instance._meta.get_field('status').choices
        self.assertEqual(choices, expected_loan_status)

    def test_loan_status_enum_contains_m_l_a_r(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        expected_loan_status = (('m', 'Maintenance'),
                                ('l', 'Loaned'),
                                ('a', 'Available'),
                                ('r', 'Reserved'),)

        self.assertEqual(book_instance.LOAN_STATUS, expected_loan_status)

    # Borrower field
    def test_borrower_is_nullable(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        is_nullable = book_instance._meta.get_field('borrower').null
        self.assertTrue(is_nullable)

    def test_borrower_can_be_blank(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        can_be_blank = book_instance._meta.get_field('borrower').blank
        self.assertTrue(can_be_blank)

    def test_borrower_relation_type(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        self.assertIsInstance(book_instance.borrower, User)

    def test_borrower_fk_relation_exists(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        relation_exists = BookInstance.objects.filter(borrower__exact=book_instance.borrower).exists()
        self.assertTrue(relation_exists)

    def test_borrower_on_delete_is_set_null(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.borrower.delete()
        book_instance.refresh_from_db()
        self.assertIsNone(book_instance.borrower)

    # Reserver field
    def test_reserver_is_nullable(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        is_nullable = book_instance._meta.get_field('reserver').null
        self.assertTrue(is_nullable)

    def test_reserver_can_be_blank(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        can_be_blank = book_instance._meta.get_field('reserver').blank
        self.assertTrue(can_be_blank)

    def test_reserver_relation_type(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        self.assertIsInstance(book_instance.reserver, User)

    def test_reserver_fk_relation_exists(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        relation_exists = BookInstance.objects.filter(reserver__exact=book_instance.reserver).exists()
        self.assertTrue(relation_exists)

    def test_reserver_on_delete_is_set_null(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.reserver.delete()
        book_instance.refresh_from_db()
        self.assertIsNone(book_instance.reserver)

    # Due back field
    def test_due_back_is_nullable(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        is_nullable = book_instance._meta.get_field('due_back').null
        self.assertTrue(is_nullable)

    def test_due_back_can_be_blank(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        can_be_blank = book_instance._meta.get_field('due_back').blank
        self.assertTrue(can_be_blank)

    # Has been renewed field
    def test_has_been_renewed_default_is_false(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        default = book_instance._meta.get_field('has_been_renewed').default
        self.assertFalse(default)

    # Methods
    def test_object_name_is_uuid_space_title_in_parenthesis(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        expected_object_name = f'{book_instance.id} ({book_instance.book.title})'
        self.assertEqual(expected_object_name, str(book_instance))

    # Meta
    def test_ordering_by_due_back(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        ordering = book_instance._meta.ordering
        expected_ordering = ['due_back']
        self.assertEqual(ordering, expected_ordering)

    def test_permissions(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        permissions = book_instance._meta.permissions
        expected_permissions = (("can_mark_returned", "Set book as returned"),
                                ("can_see_own_borrowed_books", "See borrowed books"),
                                ("can_borrow_books", "Can borrow books"),
                                ("can_renew", "Can renew a book loan"),
                                )
        self.assertEqual(permissions, expected_permissions)

    # Is overdue property
    def test_bookinst_with_future_due_date_is_no_overdue(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.due_back = datetime.date.today() + datetime.timedelta(days=1)
        self.assertFalse(book_instance.is_overdue)

    def test_bookinst_with_present_due_date_is_no_overdue(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.due_back = datetime.date.today()
        self.assertFalse(book_instance.is_overdue)

    def test_bookinst_with_past_due_date_is_overdue(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.due_back = datetime.date.today() - datetime.timedelta(days=1)
        self.assertTrue(book_instance.is_overdue)

    # Is reserved property
    def test_bookinst_with_reserver_is_reserved(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        self.assertTrue(book_instance.is_reserved)

    def test_bookinst_without_reserver_is_not_reserved(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.reserver = None
        self.assertFalse(book_instance.is_reserved)

    # Is renewable property
    def test_bookinst_reserved_is_not_renewable(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        self.assertFalse(book_instance.is_renewable)

    def test_bookinst_already_renewed_is_not_renewable(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.has_been_renewed = True
        self.assertFalse(book_instance.is_renewable)

    def test_bookinst_neither_renewed_nor_reserved_is_renewable(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.has_been_renewed = False
        book_instance.reserver = None
        self.assertTrue(book_instance.is_renewable)

    # Validations
    def test_available_book_with_due_date_raises_validation_exception(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.status = 'a'
        book_instance.due_back = datetime.date.today()
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Available books may not have a "due back" date'))
        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()

    def test_loaned_book_without_due_date_raises_validation_exception(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.status = 'l'
        book_instance.due_back = None
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Available books may not have a "due back" date'))
        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()

    def test_book_in_maintenance_without_due_date_raises_validation_exception(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.status = 'm'
        book_instance.due_back = None
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Unavailable books must have a "due back" date'))
        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()

    def test_user_without_can_borrow_permission_cant_borrow_books(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.borrower = User.objects.exclude(
            groups__permissions__codename__contains='can_borrow_books').first()
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Only users with borrow permission can borrow books'))
        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()

    def test_user_without_can_borrow_permission_cant_reserve_books(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.reserver = User.objects.exclude(
            groups__permissions__codename__contains='can_borrow_books').first()
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Only users with borrow permission can reserve books'))
        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()

    def test_user_with_borrow_permission_can_reserver_books(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        authorized_reserver = User.objects.filter(
            groups__permissions__codename__contains='can_borrow_books').last()
        book_instance.reserver = authorized_reserver
        book_instance.clean()
        self.assertEqual(book_instance.reserver, authorized_reserver)

    def test_user_with_borrow_permission_can_borrow_books(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        authorized_borrower = User.objects.filter(
            groups__permissions__codename__contains='can_borrow_books').first()
        book_instance.borrower = authorized_borrower
        book_instance.clean()
        self.assertEqual(book_instance.borrower, authorized_borrower)

    def test_loaned_book_without_borrower_raises_validation_exception(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.status = 'l'
        book_instance.borrower = None
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Loaned book must be loaned to someone innit?'))

        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()

    def test_reserved_book_without_reserver_raises_validation_exception(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.status = 'r'
        book_instance.reserver = None
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('A reserved book must have a reserver'))
        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()

    def test_maintenanced_book_marked_renewed_raises_validation_exception(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.status = 'm'
        book_instance.has_been_renewed = True
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Only a loaned book can be renewed'))

        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()

    def test_reserved_book_marked_renewed_raises_validation_exception(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.status = 'r'
        book_instance.has_been_renewed = True
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Only a loaned book can be renewed'))
        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()

    def test_available_book_marked_renewed_raises_validation_exception(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.status = 'a'
        book_instance.has_been_renewed = True
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_('Only a loaned book can be renewed'))
        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()

    def test_book_borrowed_and_reserved_by_same_user_raises_validation_exception(self):
        book_instance = BookInstance.objects.get(id=self.bookinst_id)
        book_instance.borrower = book_instance.reserver
        self.assertRaisesMessage(expected_exception=ValidationError,
                                 expected_message=_("The same user can't borrow and reserve a book at the same time"))

        with self.assertRaises(expected_exception=ValidationError):
            book_instance.clean()
