import datetime

from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType

from catalog.models import Author, Language, Genre, Book, BookInstance


# https://coverage.readthedocs.io/en/latest/
# https://adamj.eu/tech/2019/04/30/getting-a-django-application-to-100-percent-coverage/
# Remplacer commande "python" par "coverage run"
# Pour définir la portée du rapport: "--source=." pour tout
#                                    "--source=catalog" pour l'app "catalog"
# coverage run --source=catalog manage.py test catalog.tests.tests_model
# coverage report


def create_author_object(author_id, first_name, last_name, born=None, dead=None):
    author, created = Author.objects.get_or_create(id=author_id, first_name=first_name, last_name=last_name,
                                                   date_of_birth=born, date_of_death=dead)
    return author


def create_language_object(language_id, name):
    language, created = Language.objects.get_or_create(id=language_id, name=name)
    return language


def create_bookinstance_object(bookinstance_id, book, imprint, status, borrower=None, reserver=None,
                               due_back=None, has_been_renewed=False):
    return BookInstance.objects.create(id=bookinstance_id, book=book, imprint=imprint, status=status, borrower=borrower,
                                       reserver=reserver, due_back=due_back, has_been_renewed=has_been_renewed)


def create_book_object(book_id, title, isbn, summary, language, author, **kwargs):
    book, was_created = Book.objects.get_or_create(id=book_id, title=title, isbn=isbn, summary=summary,
                                                   language=language,
                                                   author=author)
    if was_created:
        genres = kwargs.get('genres')
        if genres:
            for genre in genres:
                book.genre.get_or_create(id=genre.id, name=genre.name)
    return book


def generate_a_book():
    genres = (Genre(id=2, name="Thriller"), Genre(id=3, name="Drama"), Genre(id=4, name="Fantasy"))
    language = create_language_object(language_id=1, name='English')
    author = create_author_object(author_id=1, first_name='Big', last_name='Bob', born='1950-12-25',
                                  dead='2020-11-30')

    return create_book_object(book_id=1, title="My Big Story", isbn="1234567890123",
                              summary="The summary of my super duper awesome story book", language=language,
                              author=author, **{'genres': genres})


def create_permission_object(name, codename, model):
    content_type = ContentType.objects.get_for_model(model)
    permission, created = Permission.objects.get_or_create(name=name, codename=codename, content_type=content_type)
    return permission


def create_group_object(name, permissions):
    group, created = Group.objects.get_or_create(name=name)
    if created:
        group.permissions.set(permissions)
    return group


def generate_a_librarian():
    user = User.objects.create_user(username='librarian', first_name='Johnny', last_name='Bookfinder',
                                    email='librarian@locallibrary.com',
                                    password='SuperSTR0ngP4ssW0rd!!')
    permissions = (
        create_permission_object(codename='can_renew', name='Can renew a book loan', model=BookInstance),
        create_permission_object(codename='can_mark_returned', name='Set book as returned',
                                 model=BookInstance),
        create_permission_object(codename='add_author', name='Can add author', model=Author),
        create_permission_object(codename='change_author', name='Can change author', model=Author),
        create_permission_object(codename='delete_author', name='Can delete author', model=Author),
    )

    user.groups.add(create_group_object(name='Librarians', permissions=permissions))
    user.save()
    return user


def generate_a_library_member(first_name, last_name):
    username = '_'.join((first_name[0], last_name)).lower()
    user = User.objects.create_user(username=username, first_name=first_name, last_name=last_name,
                                    email=f'{username}@internet.com',
                                    password='SuperSTR0ngP4ssW0rd!!')
    library_members_perms = (
        create_permission_object(codename='can_borrow_books', name='Can borrow books',
                                 model=BookInstance),
        create_permission_object(codename='can_see_own_borrowed_books', name='See borrowed books',
                                 model=BookInstance),
    )
    user.groups.add(create_group_object(name='Library Members', permissions=library_members_perms))
    return user


def create_a_pannel_of_authors(number):
    for author_id in range(number):
        Author.objects.create(
            first_name=f'Christian {author_id}',
            last_name=f'Surname {author_id}',
        )


def create_a_pannel_of_books(number):
    for book_id in range(number):
        Book.objects.create(
            title=f'The Story of {book_id}',
            isbn=f'1234567890123',
            summary=f'This book tells the incredible story behind the number{book_id}.',
        )


def create_a_pannel_of_book_instances(number, book=None, borrowers=None, reservers=None):
    for book_instance_id in range(number):
        status = 'm'
        due_back = datetime.date.today() + datetime.timedelta(days=book_instance_id % 5)
        borrower = None
        reserver = None
        # If no borrower: even books are under maintenance, odd books are available
        if borrowers is None and book_instance_id % 2:
            status = 'a'
            due_back = None
        # If reservers: for each loop, we take the next reserver in the tuple passed
        if reservers:
            status = 'r'
            number_reservers = len(reservers)
            reserver_id = book_instance_id % number_reservers
            reserver = reservers[reserver_id]
        # If borrowers: for each loop, we take the next borrower in the tuple passed
        if borrowers:
            status = 'l'
            number_borrowers = len(borrowers)
            borrower_id = book_instance_id % number_borrowers
            borrower = borrowers[borrower_id]

        BookInstance.objects.create(
            imprint='PaperBack',
            status=status,
            due_back=due_back,
            book=book,
            borrower=borrower,
            reserver=reserver,
        )
