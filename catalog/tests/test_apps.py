from django.test import SimpleTestCase
from catalog.apps import CatalogConfig


class CatalogAppTest(SimpleTestCase):
    def test_catalog_app_name_is_catalog(self):
        catalog_app = CatalogConfig
        self.assertEqual(catalog_app.name, 'catalog')
