import datetime
import uuid

from django.test import SimpleTestCase, TestCase
from django.utils import timezone
from catalog.forms import RenewBookForm, RenewBookModelForm
from catalog.models import BookInstance


# Here we don't actually use the database or test client.
# We can use SimpleTestCase instead of TestCase.
class RenewBookFormTest(SimpleTestCase):
    def test_renew_form_date_field_label(self):
        form = RenewBookForm()
        self.assertTrue(
            form.fields['renewal_date'].label is None or form.fields['renewal_date'].label == 'renewal date')

    def test_renew_form_date_field_help_text(self):
        form = RenewBookForm()
        self.assertEqual(form.fields['renewal_date'].help_text, 'Enter a date between now and 4 weeks (default 3).')

    def test_renew_form_date_in_past(self):
        date = datetime.date.today() - datetime.timedelta(days=1)
        form = RenewBookForm(data={'renewal_date': date})
        self.assertFalse(form.is_valid())

    def test_renew_form_date_today(self):
        date = datetime.date.today()
        form = RenewBookForm(data={'renewal_date': date})
        self.assertTrue(form.is_valid())

    def test_renew_form_date_max(self):
        date = timezone.localtime() + datetime.timedelta(weeks=4)
        form = RenewBookForm(data={'renewal_date': date})
        self.assertTrue(form.is_valid())

    def test_renew_form_date_too_far_in_future(self):
        date = datetime.date.today() + datetime.timedelta(weeks=4) + datetime.timedelta(days=1)
        form = RenewBookForm(data={'renewal_date': date})
        self.assertFalse(form.is_valid())


class RenewBookModelFormTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        BookInstance.objects.create(id=uuid.uuid4(), imprint='PaperBack', due_back=datetime.date.today())

    def test_renew_form_date_field_label(self):
        book_instance = BookInstance.objects.first()
        form = RenewBookModelForm(instance=book_instance)
        self.assertTrue(
            form.fields['due_back'].label is None or form.fields['due_back'].label == 'Renewal date')

    def test_renew_form_date_field_help_text(self):
        book_instance = BookInstance.objects.first()
        form = RenewBookModelForm(instance=book_instance)
        self.assertEqual(form.fields['due_back'].help_text, 'Enter a date between now and 4 weeks (default 3).')

    def test_renew_form_date_in_past(self):
        last_week = datetime.date.today() - datetime.timedelta(weeks=1)
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        book_instance = BookInstance.objects.first()
        book_instance.due_back = last_week
        form = RenewBookModelForm(instance=book_instance, data={'due_back': yesterday})
        self.assertFalse(form.is_valid())

    def test_renew_form_date_tomorrow(self):
        date = datetime.date.today() + datetime.timedelta(days=1)
        book_instance = BookInstance.objects.first()
        form = RenewBookModelForm(instance=book_instance, data={'due_back': date})
        self.assertTrue(form.is_valid())

    def test_renew_form_date_max_authorized_date(self):
        date = timezone.localtime() + datetime.timedelta(weeks=4)
        book_instance = BookInstance.objects.first()
        form = RenewBookModelForm(instance=book_instance, data={'due_back': date})
        self.assertTrue(form.is_valid())

    def test_renew_form_date_before_original_due_date(self):
        next_week = datetime.date.today() + datetime.timedelta(weeks=1)
        book_instance = BookInstance.objects.first()
        book_instance.due_back = next_week
        form = RenewBookModelForm(instance=book_instance, data={'due_back': datetime.date.today()})
        self.assertFalse(form.is_valid())

    def test_renew_form_date_too_far_in_future(self):
        date = datetime.date.today() + datetime.timedelta(weeks=4) + datetime.timedelta(days=1)
        book_instance = BookInstance.objects.first()
        form = RenewBookModelForm(instance=book_instance, data={'due_back': date})
        self.assertFalse(form.is_valid())

    def test_renew_form_overdue_book_date_too_far_in_future(self):
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        future_date = datetime.date.today() + datetime.timedelta(weeks=4)
        book_instance = BookInstance.objects.first()
        book_instance.due_back = yesterday
        form = RenewBookModelForm(instance=book_instance, data={'due_back': future_date})
        self.assertFalse(form.is_valid())
