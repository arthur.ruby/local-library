import datetime

from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _

from catalog.models import BookInstance


class RenewBookForm(forms.Form):
    renewal_date = forms.DateField(help_text="Enter a date between now and 4 weeks (default 3).")

    def clean_renewal_date(self):
        data = self.cleaned_data['renewal_date']

        # Check if a date is not in the past.
        if data < datetime.date.today():
            raise ValidationError(_('Invalid date - renewal cannot be in the past'))

        # Check if a date is in the allowed range (+4 weeks from today).
        if data > datetime.date.today() + datetime.timedelta(weeks=4):
            raise ValidationError(_('Invalid date - renewal cannot be more than 4 weeks ahead from now'))

        # Remember to always return the cleaned data.
        return data


class RenewBookModelForm(ModelForm):

    def clean_due_back(self):
        new_due_date = self.cleaned_data['due_back']
        book_inst = self.instance

        # Check if a date is not in the past.
        if new_due_date < datetime.date.today():
            raise ValidationError(message=_('Invalid date - renewal in past'), code='invalid')

        # Check if the date is not before the original due date
        if new_due_date < book_inst.due_back:
            raise ValidationError(message=_('Invalid date - renewal before original due date'), code='invalid')

        # Check if a date is in the allowed range:
        if book_inst.is_overdue:
            # Max +4 weeks from original due date if the book is overdue
            if new_due_date > book_inst.due_back + datetime.timedelta(weeks=4):
                raise ValidationError(message=_('Invalid date - renewal more than 4 weeks ahead from original due date'),
                                      code='invalid')
        else:
            # Or max +4 weeks from today
            if new_due_date > datetime.date.today() + datetime.timedelta(weeks=4):
                raise ValidationError(message=_('Invalid date - renewal more than 4 weeks ahead from now'),
                                      code='invalid')

        # Remember to always return the cleaned form data.
        return new_due_date

    class Meta:
        model = BookInstance
        fields = ['due_back']
        labels = {'due_back': _('Renewal date')}
        help_texts = {'due_back': _('Enter a date between now and 4 weeks (default 3).')}
