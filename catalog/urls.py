from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('books/', views.BookListView.as_view(), name='books'),
    path('book/<int:pk>', views.BookDetailView.as_view(), name='book-detail'),
    path('authors/', views.AuthorListView.as_view(), name='authors'),
    re_path(r'^author/(?P<pk>\d+)$', views.AuthorDetailView.as_view(), name='author-detail'),
    path('mybooks/', views.BorrowedBooksListView.as_view(), name='my-borrowed'),
    path('myreservations/', views.ReservedBooksListView.as_view(), name='my-reserved'),
    path('book/<uuid:pk>/cancel-reservation/', views.cancel_reservation, name='cancel-res'),
    path('borrowedbooks/', views.LoanedBooksListView.as_view(), name='all-borrowed'),
    path('book/<uuid:pk>/borrow/', views.borrow_book, name='borrow-book'),
    path('book/<uuid:pk>/reserve/', views.reserve_book, name='reserve-book'),
    path('book/<uuid:pk>/renew/', views.renew_book_with_modelform, name='renew-book-librarian'),
    path('book/<uuid:pk>/return/', views.return_loaned_book, name='return-book'),
    path('author/create/', views.AuthorCreate.as_view(), name='author-create'),
    path('author/<int:pk>/update/', views.AuthorUpdate.as_view(), name='author-update'),
    path('author/<int:pk>/delete/', views.AuthorDelete.as_view(), name='author-delete'),
]
